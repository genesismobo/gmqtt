# MQTT5 packet encoder/decoder without std or alloc

## Connect packet test query

```bash
mosquitto_pub -m "test" -t "test" -i test_id \
    -u iz \
    -P 1234 \
    --will-topic "will" \
    --will-payload "lastwill" \
    --will-qos 2 \
    --will-retain \
    -D will response-topic "will_respond" \
    -D will content-type "text" \
    -D will correlation-data "1234" \
    -D will message-expiry-interval 120 \
    -D will payload-format-indicator 2 \
    -D will user-property "sfilename" "test.txt" \
    -D will will-delay-interval 5 -V 5
```



## Writing a packet to a buffer, and reading it back
```rust
use gmqtt::{
    control_packet::{
        connect::{ConnectProperties, Login},
        Connect, Packet,
    },
    read_packet, write_packet,
};

const MAX_MQTT_PACKET_SIZE: u32 = 1024;

pub fn main() {
    let keep_alive: u16 = 60; // 60 seconds
    let login = Login {
        username: "username",
        password: "password".as_bytes(),
    };
    let properties = ConnectProperties {
        topic_alias_max: Some(0),
        max_packet_size: Some(MAX_MQTT_PACKET_SIZE),
        ..ConnectProperties::default()
    };
    let connect_packet = Packet::Connect(Connect {
        protocol: gmqtt::Protocol::V5,
        clean_start: false,
        keep_alive,
        client_id: "our client id",
        last_will: None,
        login: Some(login),
        properties: Some(properties),
    });

    let mut buffer = [0x00u8; MAX_MQTT_PACKET_SIZE as usize];

    let len = write_packet(&connect_packet, &mut buffer).unwrap();

    let read_packet = read_packet(&buffer[..len]).unwrap();

    assert_eq!(connect_packet, read_packet);
}

```
