use gmqtt::{
    control_packet::{
        connect::{ConnectProperties, Login},
        Connect, Packet,
    },
    read_packet, write_packet,
};

const MAX_MQTT_PACKET_SIZE: u32 = 1024;

pub fn main() {
    let keep_alive: u16 = 60; // 60 seconds
    let login = Login {
        username: "username",
        password: "password".as_bytes(),
    };
    let properties = ConnectProperties {
        topic_alias_max: Some(0),
        max_packet_size: Some(MAX_MQTT_PACKET_SIZE),
        ..ConnectProperties::default()
    };
    let connect_packet = Packet::Connect(Connect {
        protocol: gmqtt::Protocol::V5,
        clean_start: false,
        keep_alive,
        client_id: "our client id",
        last_will: None,
        login: Some(login),
        properties: Some(properties),
    });

    let mut buffer = [0u8; MAX_MQTT_PACKET_SIZE as usize];

    let len = write_packet(&connect_packet, &mut buffer).unwrap();

    let read_packet = read_packet(&buffer[..len]).unwrap();

    assert_eq!(connect_packet, read_packet);
}
